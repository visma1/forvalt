/* eslint-disable react-hooks/exhaustive-deps */
import { ethers, Signer } from 'ethers';
import { Box, DataTable, Heading, Text } from 'grommet';
import React, { useEffect, useState } from 'react';
import { useHistory } from "react-router-dom";
import { CapTableFactory } from '../contracts/CapTableFactory';
import { CapTableQue } from '../contracts/CapTableQue';
import { CapTableQueFactory } from '../contracts/CapTableQueFactory';
import { getWeb3 } from '../utils/web3';
import { FormatAddress } from './ui/FormatAddress';

interface Props { }

export const QueList: React.FC<Props> = () => {
    const [capTableQue, setCapTableQue] = useState<CapTableQue>();
    const [data, setData] = useState<{ address: string }[]>();
    const history = useHistory();
    const [pageOffset, /* setListOffset */] = useState(0);
    const [pageLimit, /* setpageSize */] = useState(10);

    const setList = async (list: string[]) => {
        const signer = await getWeb3();
        const dataPromises = list.reverse().slice(pageOffset, Math.min(list.length, pageOffset + pageLimit)).map(address => capTableData(address, signer))
        const data = await Promise.all(dataPromises)
        setData(data)
    }

    const capTableData = async (address: string, signer: Signer) => {
        const capTable = new CapTableFactory(signer).attach(address);
        const name = await capTable.name().catch(() => "No company found");
        const symbol = await capTable.symbol().catch(() => "No symbol found");
        const totalSupplyBN = await capTable
            .totalSupply()
            .catch(() => ethers.constants.Zero);
        const totalSupply = ethers.utils.formatEther(totalSupplyBN);
        return { address, name, symbol, totalSupply };
    };

    useEffect(() => {
        const doAsync = async () => {
            const signer = await getWeb3();
            setCapTableQue(new CapTableQueFactory(signer).attach(process.env.REACT_APP_QUE || "0x6d20198614407a4d3268F189b6eeF91f09c72149"))
        }
        doAsync()
    }, []);

    const updateList = async (type: "qued" | "approved" | "declined" | string) => {
        console.log("updateList call", type);
        if (capTableQue) {
            if (type === "qued")
                setList(await capTableQue.listQued().catch(() => []))
            else if (type === "approved")
                setList(await capTableQue.listApproved().catch(() => []))
            else if (type === "declined")
                setList(await capTableQue.listDeclined().catch(() => []))
            else
                setList([])
        }
    }

    // Initial list fetch
    useEffect(() => {
        if (capTableQue) {
            updateList("qued")
        }
    }, [capTableQue])


    return (
        <>
            <Heading>Velg selskap</Heading>
            <Box direction="row" margin="medium">
                {/* <Select
                    name="list"
                    options={[{
                        value: 'qued',
                        label: 'Kø'
                    }, {
                        value: 'approved',
                        label: 'Godkjent'
                    }, {
                        value: 'declined',
                        label: 'Avslått'
                    }]}
                    labelKey={(option) => option.label}
                    onChange={(event) => {
                        updateList(event.option.value)
                    }}
                    size="small"
                /> */}
            </Box>
            {data?.length
                ?
                <DataTable
                    onClickRow={(event: any) => {
                        history.push("/captable/" + event.datum.address);
                    }}
                    columns={[
                        {
                            property: 'symbol',
                            header: <Text>Ticker</Text>,
                        },
                        {
                            property: 'address',
                            header: <Text truncate>Address</Text>,
                            primary: true,
                            render: (datum) => <FormatAddress address={datum.address} />
                        },
                        {
                            property: 'name',
                            header: <Text>Name</Text>,
                            "search": true,
                        },
                        {
                            property: 'totalSupply',
                            header: <Text>Antall aksjer</Text>,
                        },

                    ]}
                    data={data}
                />
                :
                <Text>Tom liste</Text>
            }
        </>
    )
}