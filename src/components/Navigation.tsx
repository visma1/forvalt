import { Box, Button, Header, Heading, Text } from 'grommet';
import React from 'react';
import { Link } from "react-router-dom";
interface Props { }

export const Naviagation: React.FC<Props> = () => {
    // const size = React.useContext(ResponsiveContext);
    return (
        <Header background="background-front" alignContent="between">
            <Box direction="row" align="center" margin={{ left: "small" }}>
                <Link to="/" style={{ textDecoration: "none" }}>
                    <Heading level="2" color="white" style={{ textDecoration: "none", textTransform: "none", fontWeight: "lighter" }}>VISMA BOARDROOM</Heading>
                </Link>
                <Box margin="large">
                    <Text size="large" >|</Text>
                </Box>
                <Box margin={{ right: "small" }} direction="row" gap="medium">
                    <Link to="/captable/create">
                        <Button size="large" label="Lag selskap" hoverIndicator focusIndicator={false} />
                    </Link>
                    <Link to="/que/list">
                        <Button size="large" label="Se dine selskaper" hoverIndicator focusIndicator={false} />
                    </Link>
                    <Link to="/que/list">
                        <Button size="large" label="Oversikt" hoverIndicator focusIndicator={false} />
                    </Link>
                </Box>
            </Box>


        </Header>
    )
}