import React, { useState, useEffect } from 'react';
import { Box, Text, TextInput, Button, Grid } from 'grommet';
import { Clear, Checkmark } from 'grommet-icons';
import { getWeb3, TX_OVERRIDE } from '../../utils/web3';
import { CapTableQueFactory } from '../../contracts/CapTableQueFactory';
import { CapTableQue } from '../../contracts/CapTableQue';
import { ethers } from 'ethers';

interface Props {
    address: string
}

export const QueDetails: React.FC<Props> = ({ address }) => {

    const [que, setQue] = useState<CapTableQue>();
    const [queData, setQueData] = useState<any>();

    // Get Que
    useEffect(() => {
        const doAsync = async () => {
            const signer = await getWeb3();
            setQue(new CapTableQueFactory(signer).attach(process.env.REACT_APP_QUE || "0xC841dCc53D20560c77F0C6d858383568CDf96182"))
        }
        doAsync()
    }, []);

    // Get Que data 
    useEffect(() => {
        const doAsync = async () => {
            if (que && address) {
                const statusBN = await que.getStatus(address).catch(() => ethers.constants.Zero);
                const statusNumber = parseInt(ethers.utils.formatUnits(statusBN, 0))
                const status = () => {
                    if (statusNumber === 0) return "Ikke i kø"
                    if (statusNumber === 1) return "I kø"
                    if (statusNumber === 2) return "Akkseptert"
                    if (statusNumber === 4) return "Avslått"
                }
                setQueData({ status })
            }
        };
        doAsync();
    }, [que, address])

    const [queReason, setqueReason] = useState<string>();
    const processQue = async (decision: boolean) => {
        if (que && address && queReason) {
            const reasonBytes32 = ethers.utils.formatBytes32String(queReason)
            console.log(address, decision, reasonBytes32);

            await que.process(address, decision, reasonBytes32, TX_OVERRIDE())
        }
    }



    return (
        <Box gap="small">
            {queData &&
                <Grid columns={["small", "flex"]}>
                    <Text >Status</Text>
                    <Text weight="bold">{queData.status()}</Text>
                </Grid>
            }
            {queData && queData.status() === "I kø" &&
                <Grid columns={["small", "flex"]}>
                    <Text >Behandle</Text>
                    <Box gap="medium">
                        <TextInput
                            placeholder="Årsak..."
                            value={queReason}
                            onChange={event => setqueReason(event.target.value)}
                            size="small"
                        />
                        <Button
                            icon={<Clear />}
                            color="red"
                            label="Avslå"
                            onClick={() => processQue(false)}
                            size="small"
                            disabled={!queReason || queReason === ""}
                        />
                        <Button
                            icon={<Checkmark />}
                            color="green"
                            label="Godkjenn"
                            onClick={() => processQue(true)}
                            size="small"
                            disabled={!queReason || queReason === ""}
                        />
                    </Box>
                </Grid>

            }

        </Box>
    )
}