import React, { useState, useEffect } from 'react';
import { Button, Text, Box } from 'grommet';
import { ethers } from 'ethers';
import { FormatAddressAsName } from './ui/FormatAddressAsName';
import { getWeb3 } from '../utils/web3';
import { useAuthOracle } from '../utils/contracts';

interface Props { }

export const Account: React.FC<Props> = () => {
    const [account, setAccount] = useState<string | null>(null);
    const [name, setName] = useState<string | null>(null);
    const [authOracle] = useAuthOracle()

    // get account
    const handleLogin = async () => {
        const web3 = await getWeb3()
        const address = await web3.getAddress()
        setAccount(address)
    }

    // get name
    useEffect(() => {
        const doAsync = async () => {
            if (account !== null && authOracle) {

                const nameBytes32 = await authOracle.get_name_from_address(account).catch(err => {
                    console.log(err)
                    return ethers.utils.formatBytes32String("Fant ikke navn")
                })

                if (nameBytes32 !== null) {
                    const name = ethers.utils.parseBytes32String(nameBytes32)
                    setName(name)
                } else {
                    setName("Vennligst attester deg")
                }
            }
        };
        doAsync();
    }, [account, authOracle])

    return (
        <Box gap="small" direction="row" >
            {!account &&
                <Button label="Log inn" onClick={e => handleLogin()} plain size="small"></Button>
            }
            {!account &&
                <Button label="Skaff Metamask" onClick={e => { window.location.href = "https://metamask.io/download.html" }} plain size="small"></Button>
            }
            {account &&
                <FormatAddressAsName address={account} />
            }
            {name &&
                <Text size="small" > {name}</Text>
            }
        </Box>
    )
}