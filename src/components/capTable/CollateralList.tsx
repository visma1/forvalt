import React, { useState, useEffect } from 'react';
import { Box, Text } from 'grommet';
import { getPropertyCDP } from '../../utils/contracts';
import { ethers } from 'ethers';
import { FormatAddress } from '../ui/FormatAddress';

interface Props {
    address: string
    tokenHolderList: string[]
}

const ERC1400_TOKENS_RECIPIENT = ethers.utils.solidityKeccak256(
    ["string"],
    ["ERC1400TokensRecipient"]
);
const ERC1820_ACCEPT_MAGIC = ethers.utils.solidityKeccak256(
    ["string"],
    ["ERC1820_ACCEPT_MAGIC"]
)

export const CollateralList: React.FC<Props> = ({ address, tokenHolderList: tokenHolders }) => {
    const [mortgageHolders, setMortgageHolders] = useState<string[]>([]);


    useEffect(() => {
        const doAsync = async () => {
            let cdpHolders: string[] = []
            await Promise.all(tokenHolders
                .map(async holder => {
                    try {
                        const maybePropertyCDP = await getPropertyCDP(holder)
                        const bytesAcceptMagicMaybe = await maybePropertyCDP.canImplementInterfaceForAddress(ERC1400_TOKENS_RECIPIENT, holder)
                        console.log(bytesAcceptMagicMaybe === ERC1820_ACCEPT_MAGIC);
                        if (bytesAcceptMagicMaybe === ERC1820_ACCEPT_MAGIC) {
                            cdpHolders.push(holder)
                            setMortgageHolders(old => [...old.filter(v => v !== holder), holder])
                        }
                    } catch (error) {
                        return false
                    }
                }))
        };
        doAsync();
    }, [tokenHolders])

    useEffect(() => {
        console.log(mortgageHolders);
    }, [mortgageHolders])

    return (
        <Box>
            {mortgageHolders.map(holder => (
                <Box>
                    <Text><FormatAddress address={holder}></FormatAddress></Text>
                </Box>
            ))}
        </Box>
    )
}