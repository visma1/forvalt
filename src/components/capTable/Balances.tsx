import { ethers } from 'ethers';
import { BigNumber } from 'ethers/utils';
import { Box, Button, DataTable, Grid, Text } from 'grommet';
import React, { useEffect, useState } from 'react';
import { CapTable } from '../../contracts/CapTable';
import { CapTableFactory } from '../../contracts/CapTableFactory';
import { getERC20MintableBurnable, getPropertyCDP } from '../../utils/contracts';
import { formatBN } from '../../utils/numbers';
import { getWeb3 } from '../../utils/web3';
import { FormatAddressAsName } from '../ui/FormatAddressAsName';
import { FormatAddress } from '../ui/FormatAddress';

interface Props {
    address: string,
    tokenHolders: string[]
}

const ERC1400_TOKENS_RECIPIENT = ethers.utils.solidityKeccak256(
    ["string"],
    ["ERC1400TokensRecipient"]
);
const ERC1820_ACCEPT_MAGIC = ethers.utils.solidityKeccak256(
    ["string"],
    ["ERC1820_ACCEPT_MAGIC"]
)

interface CollateralDetail {
    cToken: {
        balance: BigNumber
        holder: string
    }[],
    pToken: {
        balance: BigNumber
        holder: string
    }[],
    closed: boolean

}
interface CollateralDetails {
    [tokenHolder: string]: CollateralDetail
}



export const Balances: React.FC<Props> = ({ address, tokenHolders }) => {
    const [capTable, setCapTable] = useState<CapTable>();
    const [account, setAccount] = useState("");
    const [tokenHoldersOffset, /* setTokenHoldersOffset */] = useState(0);
    const [tokenHolderPageLimit, /* setTokenHolderPageLimit */] = useState(10);
    const [tokenHolderData, setTokenHolderData] = useState<{ address: string, balance: BigNumber }[]>([]);
    const [tokenHolderDataDetails, setTokenHolderDataDetails] = useState<{ [tokenHolder: string]: { partition: string, balance: BigNumber }[] }>({});
    const [partitions, setPartitions] = useState<string[]>([]);
    const [collateralHolders, setCollateralHolders] = useState<string[]>([]);
    const [collateralDetails, setCollateralDetails] = useState<CollateralDetails>({});

    // Get CapTable
    useEffect(() => {
        const doAsync = async () => {
            const signer = await getWeb3();
            setCapTable(new CapTableFactory(signer).attach(address));
            setAccount(await signer.getAddress())
        };
        doAsync();
    }, [address])


    // Get CapTable data
    useEffect(() => {
        const doAsync = async () => {
            if (capTable) {
                let partitionsBytes32 = await capTable.totalPartitions()
                setPartitions(partitionsBytes32)
            }
        };
        doAsync();
    }, [address, capTable])

    // balance data
    useEffect(() => {
        const doAsync = async () => {
            if (capTable && tokenHolders) {
                const tokenHolderData = await Promise.all(tokenHolders.slice(tokenHoldersOffset, Math.min(tokenHolders.length, tokenHoldersOffset + tokenHolderPageLimit)).map(async tokenHolder => {
                    return {
                        address: tokenHolder,
                        balance: await capTable.balanceOf(tokenHolder),
                        balanceByPartition: []
                    }
                }))
                setTokenHolderData(tokenHolderData)
            }
        };
        doAsync();
    }, [capTable, tokenHolderPageLimit, tokenHolders, tokenHoldersOffset])

    // cdp data
    useEffect(() => {
        const doAsync = async () => {
            const holderPromises = tokenHolders.map(async holder => {
                try {
                    const maybePropertyCDP = await getPropertyCDP(holder)
                    const bytesAcceptMagicMaybe = await maybePropertyCDP.canImplementInterfaceForAddress(ERC1400_TOKENS_RECIPIENT, holder)
                    console.log(bytesAcceptMagicMaybe === ERC1820_ACCEPT_MAGIC);
                    if (bytesAcceptMagicMaybe === ERC1820_ACCEPT_MAGIC)
                        return holder
                    else
                        return ""
                } catch (error) {
                    return ""
                }
            })
            const holdersWithEmpty = await Promise.all(holderPromises)
            const holders = holdersWithEmpty.filter(maybeString => maybeString)
            console.log("holders", holders);
            setCollateralHolders(holders)

        };
        const timeout = setTimeout(doAsync, 1000)
        return () => {
            clearTimeout(timeout)
        }
    }, [tokenHolders])


    const getTokenHolderDataDetails = async (address: string) => {
        if (capTable && partitions) {
            const tokenHolderDetailsData = await Promise.all(partitions.map(async partition => {
                return {
                    balance: await capTable.balanceOfByPartition(partition, address),
                    partition: partition
                }
            }))
            setTokenHolderDataDetails(old => ({ ...old, [address]: tokenHolderDetailsData }))
        }
    }

    const getTokenAddresses = async (address: string) => {
        const signer = await getWeb3();
        const token = await getERC20MintableBurnable(address)
        const logs = await signer.provider.getLogs({
            fromBlock: 0,
            address: address,
            toBlock: 'latest',
            topics: [ethers.utils.id(`Transfer(address,address,uint256)`)]
        }).finally(() => [])

        // console.log("logs => ", logs);
        const events = logs.map(log => {
            return { ...token.interface.parseLog(log) }
        })
        // console.log("events => ", events);

        const addresses = events
            .map(event => {
                return event.values.to
            })
            .reduce((prev, cur) => prev.indexOf(cur) === -1 ? [...prev, cur] : prev, []) as string[]

        console.log("addresses => ", events);
        return addresses
    }

    const getColleralDetails = async (address: string) => {
        if (capTable && partitions) {
            const propertyCDP = await getPropertyCDP(address)
            const cTokenAddress = await propertyCDP.getCToken();
            const pTokenAddress = await propertyCDP.getPToken();

            const cToken = await getERC20MintableBurnable(cTokenAddress)
            const pToken = await getERC20MintableBurnable(pTokenAddress)

            const cTokenAddresses = await getTokenAddresses(cTokenAddress)
            const pTokenAddresses = await getTokenAddresses(pTokenAddress)

            const cTokenBalances = cTokenAddresses.map(async address => {
                return {
                    holder: address,
                    balance: await cToken.balanceOf(address)
                }
            })
            const pTokenBalances = pTokenAddresses.map(async address => {
                return {
                    holder: address,
                    balance: await pToken.balanceOf(address)
                }
            })
            const collateralDetail: CollateralDetail = {
                closed: await propertyCDP.closed(),
                cToken: await Promise.all(cTokenBalances),
                pToken: await Promise.all(pTokenBalances),

            }
            setCollateralDetails(old => ({ ...old, ...{ [address]: collateralDetail } }))
        }
    }

    return (
        <>
            <Box >
                <DataTable
                    data={tokenHolderData}
                    columns={[
                        {
                            property: 'address',
                            header: <Text>Adress</Text>,
                            primary: true,
                            render: data => (
                                collateralHolders.indexOf(data.address) === -1
                                    ? <Text weight={account && account === data.address ? "bold" : "normal"}><FormatAddressAsName address={data.address} /></Text>
                                    : <Text weight={account && account === data.address ? "bold" : "normal"}>Pantet</Text>

                            )
                        },
                        {
                            property: 'balance',
                            header: <Text>Aksjer</Text>,
                            render: data => (
                                <Text truncate>{formatBN(data.balance)}</Text>
                            )
                        },
                        {
                            property: 'balanceByPartition',
                            header: <Text>Aksjeklasser</Text>,
                            render: data => (
                                <Box gap="small" pad="small" >
                                    {!tokenHolderDataDetails.hasOwnProperty(data.address) &&
                                        <Button label="Hent" size="small" onClick={() => getTokenHolderDataDetails(data.address)}></Button>
                                    }
                                    {tokenHolderDataDetails.hasOwnProperty(data.address) && tokenHolderDataDetails[data.address].filter(tokenHolderDetail => formatBN(tokenHolderDetail.balance) > 0.09).map((tokenHolderDetail, i) =>
                                        <Grid columns={["xsmall", "flex"]} gap="small" key={i} >
                                            <Text size="small" truncate>{ethers.utils.parseBytes32String(tokenHolderDetail.partition)}</Text>
                                            <Text size="small" truncate>{formatBN(tokenHolderDetail.balance)}</Text>
                                        </Grid>
                                    )}


                                </Box>
                            )
                        },
                        {
                            property: 'collateral',
                            header: <Text>Sikkerhet</Text>,
                            render: data => (
                                collateralHolders.indexOf(data.address) !== -1 &&

                                <Box gap="small" pad="small" >
                                    {!collateralDetails.hasOwnProperty(data.address) &&
                                        <Button label="Hent" size="small" onClick={() => getColleralDetails(data.address)}></Button>
                                    }
                                    {collateralDetails.hasOwnProperty(data.address) &&
                                        <Box>
                                            {collateralDetails[data.address].closed
                                                ? <Text>Lukket</Text>
                                                : <Box gap="small">
                                                    <Box>
                                                        <Text weight="bold">Eiere</Text>
                                                        {collateralDetails.hasOwnProperty(data.address) && collateralDetails[data.address].cToken
                                                            .filter(cTokenDetail => formatBN(cTokenDetail.balance) > 0.9)
                                                            .map((cTokenDetail, i) => (
                                                                <Box key={i} >
                                                                    <Grid columns={["xsmall", "flex"]} gap="small" key={i} >
                                                                        <Text size="small" truncate>{<FormatAddress address={cTokenDetail.holder}></FormatAddress>}</Text>
                                                                        <Text size="small" truncate>{formatBN(cTokenDetail.balance)}</Text>
                                                                    </Grid>
                                                                </Box>
                                                            )
                                                            )}
                                                    </Box>
                                                    <Box>
                                                        <Text weight="bold">Panthavere</Text>
                                                        {collateralDetails.hasOwnProperty(data.address) && collateralDetails[data.address].pToken.map((pTokenDetail, i) => (
                                                            <Box key={i} >
                                                                <Grid columns={["xsmall", "flex"]} gap="small" key={i} >
                                                                    <Text size="small" truncate>{<FormatAddress address={pTokenDetail.holder}></FormatAddress>}</Text>
                                                                    <Text size="small" truncate>{pTokenDetail.balance.toNumber()}</Text>
                                                                </Grid>
                                                            </Box>
                                                        )
                                                        )}
                                                    </Box>
                                                </Box>
                                            }
                                        </Box>
                                    }


                                </Box>
                            )
                        }
                    ]}
                ></DataTable>

            </Box>
        </>
    )
}