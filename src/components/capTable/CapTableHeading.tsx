import React, { useEffect, useState } from 'react';
import { Box, Heading } from 'grommet';
import { useCapTable } from '../../utils/contracts';

interface Props {
    address: string
}

export const CapTableHeading: React.FC<Props> = ({ address }) => {

    const [capTable] = useCapTable(address)
    const [name, setName] = useState("");

    useEffect(() => {
        const doAsync = async () => {
            if (capTable) {
                const name = await capTable.name()
                setName(name)
            }
        };
        doAsync();
    }, [capTable])

    return (
        <Box>
            {name
                ? <Heading level="2">{name}</Heading>
                : <Heading>Aksjeierbok</Heading>
            }

        </Box>
    )
}