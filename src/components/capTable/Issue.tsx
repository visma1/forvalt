import React, { useEffect, useState } from 'react';
import { getWeb3, TX_OVERRIDE } from '../../utils/web3';
import { CapTableFactory } from '../../contracts/CapTableFactory';
import { CapTable } from '../../contracts/CapTable';
import { TextInput, Box, Button, Select, Text } from 'grommet';
import { ethers } from 'ethers';

interface Props {
    address: string
}

export const Issue: React.FC<Props> = ({ address }) => {
    const [capTable, setCapTable] = useState<CapTable>();
    const [account, setAccount] = useState<string>();
    const [isMinter, setIsMinter] = useState(false);

    // Get CapTable
    useEffect(() => {
        const doAsync = async () => {
            const signer = await getWeb3();
            setCapTable(new CapTableFactory(signer).attach(address));
            setAccount(await signer.getAddress())
        };
        doAsync();
    }, [address])

    const [partitions, setPartitions] = useState<string[]>([]);
    // Get CapTable data
    useEffect(() => {
        const doAsync = async () => {
            if (capTable && account) {
                let partitionsBytes32 = await capTable.totalPartitions()
                if (partitionsBytes32.length === 0) {
                    partitionsBytes32 = await capTable.getDefaultPartitions().catch(() => [])
                }
                const partitions = partitionsBytes32.map(bytes32 => ethers.utils.parseBytes32String(bytes32))
                setPartitions(partitions)
                const isMinter = await capTable.isMinter(account)
                setIsMinter(isMinter)
            }
        };
        doAsync();
    }, [address, capTable, account])

    const [to, setTo] = useState<string>(process.env.NODE_ENV === "development" ? "0xC9901c379E672912D86D12Cb8f182cFaf5951940" : "");
    const [amount, setAmount] = useState<number>();
    const [partition, setPartition] = useState<string>();
    const [newPartition, setNewPartition] = useState<string>();

    // Hydrate development data
    useEffect(() => {
        if (process.env.NODE_ENV === "development") {
            setTo("0xC9901c379E672912D86D12Cb8f182cFaf5951940")
            setAmount(2)
            setPartition(partitions[0])
        }
    }, [partitions])

    const issueShares = async () => {
        if (capTable && to && amount && partition) {
            const tx = await capTable.issueByPartition(ethers.utils.formatBytes32String(partition), to, ethers.utils.parseEther(amount.toString()), "0x11", TX_OVERRIDE())
            await tx.wait()
        }
    }

    const issueForm = () => (
        <Box>
            <Box margin="small" direction="row" gap="small">
                <Box basis="100%" margin="small">
                    <TextInput
                        style={{ minWidth: "100%" }}
                        placeholder="Til addresse"
                        value={to}
                        onChange={event => setTo(event.target.value)}
                    />
                </Box>

            </Box>
            <Box margin="small" direction="row" gap="small">
                <Box basis="2/3" margin="small">
                    <Select
                        placeholder="Aksjeklasse"
                        options={partitions}
                        value={partition}
                        onChange={({ option }) => setPartition(option)}
                    />
                </Box>
                <Box basis="1/3" margin="small">
                    <TextInput
                        placeholder="Antall aksjer"
                        value={amount}
                        type="number"
                        onChange={event => setAmount(parseInt(event.target.value))}
                    />
                </Box>
            </Box>

            <Box margin="small">
                <Button label="Opprett aksjer" onClick={() => issueShares()}></Button>
            </Box>

            <Box margin="small" direction="row" gap="small">
                <Box basis="2/3" margin="small">
                    <TextInput
                        placeholder="Ny aksjeklasse"
                        value={newPartition}
                        type="text"
                        onChange={event => setNewPartition(event.target.value)}
                    />
                </Box>
                <Box basis="1/3" gap="small">
                    <Button
                        label="Foreslå ny aksjeklasse"
                        onClick={() => setPartitions(old => (newPartition ? [...old, newPartition] : old))}>

                    </Button>
                    <Text size="xsmall">Du må opprette aksjer i denne aksjeklassen for at den blir værende.</Text>
                </Box>
            </Box>
        </Box>
    )

    return (
        <>
            {!isMinter &&
                <Text>Du har ikke utstede rettigheter</Text>
            }
            {isMinter &&
                issueForm()
            }

        </>
    )

}