import { ethers } from 'ethers';
import { Box, Grid, Text } from 'grommet';
import React, { useEffect, useState } from 'react';
import { CapTable } from '../../contracts/CapTable';
import { CapTableFactory } from '../../contracts/CapTableFactory';
import { getWeb3 } from '../../utils/web3';

interface Props {
    address: string
}

export interface CapTableData {
    address: string
    name: string
    uuid: string
    totalSupply: string
}

export const Info: React.FC<Props> = ({ address }) => {

    const [account, setAccount] = useState<string>();
    const [capTable, setCapTable] = useState<CapTable>();
    const [data, setData] = useState<CapTableData>();


    // Get CapTable
    useEffect(() => {
        const doAsync = async () => {
            const signer = await getWeb3();
            setCapTable(new CapTableFactory(signer).attach(address));
            setAccount(await signer.getAddress())
        };
        doAsync();
    }, [address])

    // Get CapTable data
    useEffect(() => {
        const doAsync = async () => {
            if (capTable && account) {
                const name = await capTable.name().catch(() => "No company found");
                const totalSupplyBN = await capTable
                    .totalSupply()
                    .catch(() => ethers.constants.Zero);
                const totalSupply = ethers.utils.formatEther(totalSupplyBN);
                const uuidBytes32 = await capTable.getUuid()
                const uuid = ethers.utils.parseBytes32String(uuidBytes32)

                setData({ address, name, uuid, totalSupply });
            }
        };
        doAsync();
    }, [capTable, address, account])

    return (
        <Box gap="small">
            {data &&
                <Grid columns={["small", "flex"]}>
                    <Text>Navn</Text>
                    <Text weight="bold">{data.name}</Text>
                </Grid>
            }
            {data &&
                <Grid columns={["small", "flex"]}>
                    <Text >Orginisasjonsnummer</Text>
                    <Text weight="bold">{data.uuid}</Text>
                </Grid>
            }
            {data &&
                <Grid columns={["small", "flex"]}>
                    <Text >Antall aksjer</Text>
                    <Text weight="bold">{data.totalSupply}</Text>
                </Grid>
            }

        </Box>
    )
}