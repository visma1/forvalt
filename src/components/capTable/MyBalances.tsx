import React, { useEffect, useState } from 'react';
import { Box, List, Text } from 'grommet';
import { CapTable } from '../../contracts/CapTable';
import { getWeb3 } from '../../utils/web3';
import { CapTableFactory } from '../../contracts/CapTableFactory';
import { ethers } from 'ethers';

interface Props {
    address: string
}

export interface CapTableAccountData {
    partitionBalances: {
        partition: string,
        balance: string
    }[]
}

export const MyBalances: React.FC<Props> = ({ address }) => {
    const [accountData, setAccountData] = useState<CapTableAccountData>();
    const [account, setAccount] = useState<string>();
    const [capTable, setCapTable] = useState<CapTable>();

    // Get CapTable
    useEffect(() => {
        const doAsync = async () => {
            const signer = await getWeb3();
            setCapTable(new CapTableFactory(signer).attach(address));
            setAccount(await signer.getAddress())
        };
        doAsync();
    }, [address])


    // Get CapTableAccount data
    useEffect(() => {
        const doAsync = async () => {
            if (capTable && account) {
                const partitionsBytes32 = await capTable.partitionsOf(account).catch(() => [] as string[]);
                const partitionBalancesPromises = partitionsBytes32.map(async partitionBytes32 => {
                    const partition = ethers.utils.parseBytes32String(partitionBytes32)
                    const balanceBN = await capTable.balanceOfByPartition(partitionBytes32, account)
                    const balance = ethers.utils.formatEther(balanceBN)
                    return {
                        partition,
                        balance
                    }
                })
                const partitionBalances = await Promise.all(partitionBalancesPromises)

                setAccountData({ partitionBalances });
            }
        };
        doAsync();
    }, [capTable, account])

    return (
        accountData ?
            <Box>
                <List
                    primaryKey="partition"
                    secondaryKey="balance"
                    data={accountData.partitionBalances}
                />
            </Box>
            :
            <Text>"Henter data..."</Text>
    )
}