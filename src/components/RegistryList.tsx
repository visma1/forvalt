/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from 'react';
import { DataTable, Text, Heading } from 'grommet';
import { FormatAddress } from './ui/FormatAddress';
import { getWeb3 } from '../utils/web3';
import { CapTableFactory } from '../contracts/CapTableFactory';
import { ethers, Signer } from 'ethers';
import { useHistory } from "react-router-dom";
import { CapTableRegistry } from '../contracts/CapTableRegistry';
import { CapTableRegistryFactory } from '../contracts/CapTableRegistryFactory';

interface Props { }

export const RegistryList: React.FC<Props> = () => {
    const [registry, setRegistry] = useState<CapTableRegistry>();
    const [data, setData] = useState<{ address: string }[]>();
    const history = useHistory();

    // get registry
    useEffect(() => {
        const doAsync = async () => {
            const signer = await getWeb3();
            setRegistry(new CapTableRegistryFactory(signer).attach(process.env.REACT_APP_REGISTRY || "0x4B5e8c74a60155f2a7546151b631b82540c100c9"))
        }
        doAsync()
    }, []);

    // Fetch initial data
    useEffect(() => {
        if (registry) {
            updateList('active')
        }
    }, [registry])

    const updateList = async (type: string | 'active' | 'all' = "active") => {
        if (registry) {
            if (type === "active")
                setList(await registry.listActive().catch((err) => {
                    console.log(err);
                    return []
                }))
            if (type === "all")
                setList(await registry.list().catch(() => []))
            else
                setList([])
        }
    }

    const setList = async (list: string[]) => {
        console.log("Registry list ", list);

        const signer = await getWeb3();
        const dataPromises = list.slice(0, 10).map(address => capTableData(address, signer))
        const data = await Promise.all(dataPromises)
        setData(data)
    }

    const capTableData = async (address: string, signer: Signer) => {
        const capTable = new CapTableFactory(signer).attach(address);
        const name = await capTable.name().catch(() => "No company found");
        const symbol = await capTable.symbol().catch(() => "No symbol found");
        const totalSupplyBN = await capTable
            .totalSupply()
            .catch(() => ethers.constants.Zero);
        const totalSupply = ethers.utils.formatEther(totalSupplyBN);
        return { address, name, symbol, totalSupply };
    };


    return (
        <>
            <Heading>Aksjeeierbok register</Heading>
            {data?.length
                ?
                <DataTable
                    onClickRow={(event: any) => {
                        history.push("/captable/" + event.datum.address);
                    }}
                    columns={[
                        {
                            property: 'symbol',
                            header: <Text>Ticker</Text>,
                        },
                        {
                            property: 'address',
                            header: <Text truncate>Address</Text>,
                            primary: true,
                            render: (datum) => <FormatAddress address={datum.address}></FormatAddress>
                        },
                        {
                            property: 'name',
                            header: <Text>Name</Text>,
                            "search": true,
                        },
                        {
                            property: 'totalSupply',
                            header: <Text>Antall aksjer</Text>,
                        },

                    ]}
                    data={data}
                />
                :
                <Text>Tom liste</Text>
            }
        </>
    )
}