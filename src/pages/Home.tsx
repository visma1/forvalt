import { Box, Button, WorldMap } from 'grommet';
import React from 'react';

interface Props { }

export const Home: React.FC<Props> = () => {

    return (

        <Box fill="horizontal" background={{ "image": "url(https://boardroom-stage.vismaonline.com/65f77215a5201a94f77ac8fbee5616dd.jpg)" }}>
            <Box align="center" margin="large">

                <Box width="large" align="center" pad="large"  >
                    <WorldMap color="white" />
                </Box>
                <Button size="large" color="blue" style={{ color: "white", fontWeight: "bold" }} label="Logg inn" ></Button>
            </Box>
        </Box>

    )
}