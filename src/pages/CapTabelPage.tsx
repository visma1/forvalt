import { Box, Button, Grid, Heading, Layer } from 'grommet';
import { ChapterAdd, FormRefresh, InProgress, Transaction } from 'grommet-icons';
import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { Admin } from '../components/capTable/Admin';
import { CollateralBalances } from '../components/capTable/CollateralBalances';
import { CollateralList } from '../components/capTable/CollateralList';
import { Info } from '../components/capTable/Info';
import { Issue } from '../components/capTable/Issue';
import { Transfer } from '../components/capTable/Transfer';
import { QueDetails } from '../components/que/QueDetails';
import { UIBox } from '../components/ui/UIBox';
import { getERC1400Addresses } from '../utils/cdpHelpers';
import { Rotate } from '../utils/Rotate';
interface Props { }

export const CapTabelPage: React.FC<Props> = () => {
    const { address } = useParams<{ address: string }>();

    const [show, setShow] = useState<boolean | string>("");
    const [tokenHolderList, setTokenHolderList] = useState<string[]>();

    useEffect(() => {
        const doAsync = async () => {
            if (address) {
                const tokenHolders = await getERC1400Addresses(address)
                setTokenHolderList(tokenHolders)
            }
        };
        doAsync();
    }, [address])

    return (
        <>
            <Box>
                <Grid gap="small" columns={{ size: "small", count: 5 }} margin="large" >
                    <Button
                        label="Periodeavslutning"
                        onClick={() => setShow("issue")}

                        style={{ fontWeight: "bold" }}
                        color="brand"
                    />
                    <Button
                        label="Årsoppgjør"
                        onClick={() => setShow("issue")}

                        style={{ fontWeight: "bold" }}
                        color="brand"
                    />

                    <Button
                        label="Transaksjonsanalyse"
                        onClick={() => setShow("issue")}


                        style={{ fontWeight: "bold" }}
                        color="brand"
                    />
                    <Button
                        label="Vedlikehold"
                        onClick={() => setShow("transfer")}


                        style={{ fontWeight: "bold" }}
                        color="brand"
                    />
                    <Button
                        label="Altinn"
                        onClick={() => setShow("admin")}


                        style={{ fontWeight: "bold" }}
                        color="brand"

                    />
                </Grid>
            </Box>
            {address &&
                <Box gap="large" >
                    <UIBox heading="Aksjeregister">
                        {tokenHolderList
                            ? < CollateralBalances address={address} tokenHolderList={tokenHolderList}></CollateralBalances>
                            : <Box>
                                <Heading level={4}>Henter liste... <Rotate><FormRefresh></FormRefresh></Rotate></Heading>
                            </Box>
                        }
                    </UIBox>
                    <UIBox>
                        <Grid columns={["1/2", "1/2"]} fill="horizontal">
                            <Heading level="3">Detaljer</Heading>
                            <Info address={address}></Info>

                        </Grid>
                    </UIBox>

                    <UIBox heading="Handlinger">
                        <Grid gap="small" columns={{ size: "small", count: 3 }}>

                            <Button
                                icon={<ChapterAdd />}
                                label="Regnskap"
                                onClick={() => setShow("issue")}
                            />

                            <Button
                                icon={<ChapterAdd />}
                                label="Styremøter"
                                onClick={() => setShow("issue")}
                            />

                            <Button
                                icon={<ChapterAdd />}
                                label="Dokumenter"
                                onClick={() => setShow("issue")}
                            />



                            <Button
                                icon={<Transaction />}
                                label="Aksje overføring"
                                onClick={() => setShow("transfer")}

                            />

                            <Button
                                icon={<InProgress />}
                                label="Administrasjon"
                                onClick={() => setShow("admin")}

                            />

                            {/* <Button
                                icon={<CatalogOption />}
                                label="Pant"
                                onClick={() => setShow("collateral")}
                                style={{ borderRadius: "0px" }}
                            /> */}
                        </Grid>
                    </UIBox>



                    {show &&
                        <Layer
                            onEsc={() => setShow(false)}
                            onClickOutside={() => setShow(false)}
                            modal

                        >
                            <Box margin="large">

                                {show === "transfer" &&
                                    <Transfer address={address}></Transfer>
                                }
                                {show === "issue" &&
                                    <Issue address={address}></Issue>
                                }
                                {show === "que" &&
                                    <QueDetails address={address}></QueDetails>
                                }
                                {show === "admin" &&
                                    <Admin address={address}></Admin>
                                }
                                {show === "collateral" && tokenHolderList ?
                                    <CollateralList address={address} tokenHolderList={tokenHolderList}></CollateralList>
                                    :
                                    <Heading level={4}>Laster balanser... <Rotate><FormRefresh></FormRefresh></Rotate></Heading>
                                }
                                <Button margin="small" label="Lukk" onClick={() => setShow(false)} />
                            </Box>
                        </Layer>
                    }
                </Box>
            }
        </>
    )
}