import React from 'react';
import { Heading } from 'grommet';
import { useParams } from 'react-router-dom';
import { BatchIssue } from '../components/capTable/BatchIssue';

interface Props { }

export const OnBoardCompanyPage: React.FC<Props> = () => {
    const { address } = useParams();

    return (
        <>
            <Heading level={4}>Utsted aksjer</Heading>
            {address &&
                <BatchIssue address={address}></BatchIssue>
            }
        </>
    )
}