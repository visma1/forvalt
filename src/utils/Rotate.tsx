import React from 'react';
import styled, { keyframes } from "styled-components";

interface Props { }
const rotate = keyframes`
        from {
            transform: rotate(0deg);
        }
        to {
            transform: rotate(360deg);
        }
        `;

const RotateDiv = styled.div`
        display: inline-block;
        animation: ${rotate} 2s linear infinite;
        align: "center",
        justify: "center",
        width: "10px",
        height: "10px",
        `;


export const Rotate: React.FC<Props> = ({ children }) => {

    return (
        <RotateDiv>{children}</RotateDiv>
    )
}