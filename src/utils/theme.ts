import { ThemeType } from "grommet";
import { deepFreeze } from "grommet/utils";

export const Theme: ThemeType = deepFreeze({
  rounding: 4,
  spacing: 24,
  defaultMode: "light",
  global: {
    font: {
      family: "Quicksand, sans-serif",
    },
    colors: {
      green: {
        light: "#4f8800",
        dark: "#4f8800",
      },
      mint: {
        light: "#cdece2",
        dark: "#cdece2",
      },
      yellow: {
        light: "#E8A74A",
        dark: "#E8A74A",
      },
      red: {
        light: "#cd465e",
        dark: "#cd465e",
      },
      white: {
        light: "#ffffff",
        dark: "#ffffff",
      },
      blue: {
        light: "#0079bf",
        dark: "#006199",
      },
      grey: {
        light: "#f1f1f1",
        dark: "#f1f1f1",
      },
      black: {
        light: "#333333",
        dark: "#333333",
      },
      background: "blue",
      "background-back": "white",
      "background-front": "blue",
      "background-contrast": "white",
      brand: "blue",
      "status-ok": "green",
      "status-warning": "yellow",
      "status-error": "red",
      "status-critical": "red",
      "status-disabled": "mint",
      "status-unknown": "blue",
      focus: "green",
      text: {
        light: "#444",
        dark: "#fff",
      },
      control: "white",
      active: "brand",
      selected: "brand",
      icon: "brand",
    },
    active: {
      color: "#fff",
    },
  },
  button: {},
});
